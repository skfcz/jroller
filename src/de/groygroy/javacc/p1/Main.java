/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.groygroy.javacc.p1;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
public class Main {

    public static Logger log = LoggerFactory.getLogger( Main.class );

    public static void main( String[] args ) {



        BasicConfigurator.configure();


        try {
           

            InputStream in = new FileInputStream( "data/simple.jdl" );

            JoveDLParser parser = new JoveDLParser( in );

            SimpleNode node = parser.JoveDL();
            
            // print AST using Visitor
            JoveDLParserVisitor visitor  = new LoggerVisitor();
            visitor.visit( node, null);
            
            // Collect some values from the AST
            visitor  = new CollectorVisitor();
            visitor.visit( node, null);
            



        } catch ( Exception exp ) {
            log.error( "an error occured", exp );
            System.exit( 1 );
        }
        System.exit( 0 );

    }
}

/*
 * Options are used to configure the JavaCC and JJTree process itself
 */
options {

	/*
	 * Default value is true. If true, all methods and class variables 
	 * are specified as static in the generated parser and token manager. This allows only 
	 * one parser object to be present, but it improves the performance of the parser.
	 */
	STATIC = false;

	/*
	 * Options for obtaining debugging information
	 */
	DEBUG_PARSER = false;
	DEBUG_TOKEN_MANAGER = false;      

	/*
	 * Default value is false. When set to true, the generated parser 
	 * uses an input stream object that processes Java Unicode escapes before 
	 * sending characters to the token manager. 
	 */
	JAVA_UNICODE_ESCAPE = false;

	/*
	 * Default value is false. When set to true, the generated parser 
	 * uses uses an input stream object that reads Unicode files. By default, ASCII files 
	 * are assumed. 
	 */
	UNICODE_INPUT = false;

	/*
	 * Default value is false. Generate a multi mode parse tree. 
	 * The default for this is false, generating a simple mode parse tree.
	 */
	MULTI=true;

	/*
	 * Default value is false. Instead of making each non-decorated 
	 * production an indefinite node, make it void instead.
	 */
	NODE_DEFAULT_VOID=false;

	/*
	 * The package to generate the node classes into. 
	 */
	NODE_PACKAGE = "de.groygroy.javacc.p1";

        /*
	 * Default value is false. Insert a jjtAccept() method in the 
	 * node classes, and generate a visitor implementation with an entry for every 
	 * node type used in the grammar.
	 */
	VISITOR=true;

        /*
        * If this option is set, it is used in the signature of the generated jjtAccept() 
        * methods and the visit() methods as the type of the data argument. 
        */
        VISITOR_DATA_TYPE="java.util.Map<String, Object>";

        /*
         * If this option is set, it is used in the signature of the generated jjtAccept() 
         * methods and the visit() methods as the return type of the method. 
         */
        VISITOR_RETURN_TYPE="Object";

	/*
	 * Default value is false. Insert calls to user-defined parser 
	 * methods on entry and exit of every node scope. See Node Scope Hooks above.
	 */
	NODE_SCOPE_HOOK=true;


	/*
	 * JDK Version
	 */
	JDK_VERSION = "1.5";
}

PARSER_BEGIN(JoveDLParser)

package de.groygroy.javacc.p1;

import java.io.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The JoveDLParser class is the parser for the declaration 
 * language.It is automatically generated using JavaCC and generates an AST 
 * of the jdl files. 
 * To extract information from the AST use a {@link JoveDLVisitorAdapter}.
 * E.g.
 * <pre>
    JoveDLParser parser = new JoveDLParser( in );
    SimpleNode node = parser.JoveDL();
    in.close();
    TypeCollectorVisitorAdapter vis = new TypeCollectorVisitorAdapter();
    node.jjtAccept( vis, null );
</pre>
 */
public class JoveDLParser {

    public static String PARSER_LOGGER = "parser";
    public static Logger log = LoggerFactory.getLogger( JoveDLParser.class );
    public static Logger parserLog = LoggerFactory.getLogger( PARSER_LOGGER);
    private int indent = 0;
    
    /** 
     * This method is called during parsing and generating the AST
     */
    private void jjtreeOpenNodeScope( Node node ) {
        parserLog.info( indent(+1) + node.toString() );
    }

    /** 
     * This method is called during parsing and generating the AST
     */
    private void jjtreeCloseNodeScope( Node node ) {
        parserLog.info( indent(-1) + node.toString() );
    }

     private String indent( int delta ) {
        indent += delta;
        StringBuffer buff = new StringBuffer();

        for ( int i = 0; i < indent; i++ ) {
            buff.append( "    " );
        }
        return buff.toString();
    }

	
}

PARSER_END(JoveDLParser)


// 
// TOKEN DEFINITION
// 

/* WHITE SPACE */

SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
| "\f"
}

/* COMMENTS */
MORE : {    
  "/**" : IN_FORMAL_COMMENT
}
<IN_FORMAL_COMMENT>
MORE : {
    < ~[]>
}
<IN_FORMAL_COMMENT>
TOKEN : {
  <FORMAL_COMMENT: "*/" > {}  : DEFAULT
}

MORE :
{
  "//" : IN_SINGLE_LINE_COMMENT
|
  "/*" : IN_MULTI_LINE_COMMENT
}



<IN_SINGLE_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <SINGLE_LINE_COMMENT: "\n" | "\r" | "\r\n" > : DEFAULT
}

<IN_MULTI_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <MULTI_LINE_COMMENT: "*/" > : DEFAULT
}

<IN_SINGLE_LINE_COMMENT,IN_FORMAL_COMMENT,IN_MULTI_LINE_COMMENT>
MORE :
{
  < ~[] >
}


/* RESERVED WORDS AND LITERALS */

// Data Types
TOKEN :
{
  <UChar : "UChar" > // An unsigned 8-bit byte.
| <U8 : "U8" > // An unsigned 8-bit integer value.
| <U16 : "U16" > // An unsigned 16-bit integer value.
| <U32 : "U32" > // An unsigned 32-bit integer value.
| <U64 : "U64" > // An unsigned 64-bit integer value.
| <I16 : "I16" > // A signed two‟s complement 16-bit integer value.
| <I32 : "I32" > // A signed two‟s complement 32-bit integer value.
| <I64 : "I64" > // A signed two's complement 64-bit integer value.
| <F32 : "F32" > // An IEEE 32-bit floating point number.
| <F64 : "F64" > // An IEEE 64-bit double precision floating point number
| <GUID : "GUID" > // 
| <VecF32 : "VecF32" > //
| <BBoxF32 : "BBoxF32" > //
| <CoordF32 : "CoordF32" > //
| <MbString : "MbString" > 
}

// Structure Types
TOKEN : 
{
  <BLOCK : "block" >
| <SEGMENT : "segment" >
| <ENTITY : "entity" >
| <ABSTRACT : "abstract" >
| <EXTENDS : "extends" >
| <OBJECT_TYPE_ID: "ObjectTypeID">
| <VAR : "var" >
}

// LITERALS used as values in the key / value segment
TOKEN :
{
     < GUID_LITERAL : <HEX_LITERAL_8> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_4> (" ")* <COMMA> (" ")* 
               <HEX_LITERAL_4> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2> (" ")* <COMMA> (" ")*
               <HEX_LITERAL_2>
               >
    |  < INTEGER_LITERAL: <DECIMAL_LITERAL> (["l","L"])? >         
    | < #DECIMAL_LITERAL: ["1"-"9"] (["0"-"9"])* >
    | < #HEX_LITERAL_8: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"]){8} >
    | < #HEX_LITERAL_4: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"]){4} >
    | < #HEX_LITERAL_2: "0" ["x","X"] (["0"-"9","a"-"f","A"-"F"]){2} >
    | < #OCTAL_LITERAL: "0" (["0"-"7"])* >
    | < FLOATING_POINT_LITERAL:
        (["0"-"9"])+ "." (["0"-"9"])* (<EXPONENT>)? (["f","F","d","D"])?
      | "." (["0"-"9"])+ (<EXPONENT>)? (["f","F","d","D"])?
      | (["0"-"9"])+ <EXPONENT> (["f","F","d","D"])?
      | (["0"-"9"])+ (<EXPONENT>)? ["f","F","d","D"]
        >
    |  < #EXPONENT: ["e","E"] (["+","-"])? (["0"-"9"])+ >
    |  < STRING_LITERAL:
      "\""
      (   (~["\"","\\","\n","\r"])
        | ("\\"
            ( ["n","t","b","r","f","\\","'","\""]
            | ["0"-"7"] ( ["0"-"7"] )?
            | ["0"-"3"] ["0"-"7"] ["0"-"7"]
            )
          )
      )*
      "\""
        >
    | < BOOLEAN_LITERAL: "true" | "false" >
}

// Identifier used to name blocks, segments ...
TOKEN :
{
   <IDENTIFIER: <LETTER> (<LETTER> | <DIGIT> )*>
|  <#LETTER: ["A"-"Z", "a"-"z"]>
|  <#DIGIT: ["0"-"9"]>
}

// SEPARATORS
TOKEN :
{
  < LPAREN: "(" >
| < RPAREN: ")" >
| < LBRACE: "{" >
| < RBRACE: "}" >
| < LBRACKET: "[" >
| < RBRACKET: "]" >
| < SEMICOLON: ";" >
| < COMMA: "," >
| < DOT: "." >
| < AT: "@" >
| <COLON : ":" >
}


/*****************************************************
 * THE JOVE DESCRIPTION LANGUAGE GRAMMAR STARTS HERE *
 *****************************************************/

/*
 * Program structuring syntax follows.
 */

/**
 * This is the start method for a valid Jove DL file
 */
SimpleNode JoveDL():
{}
{
   ( (FormalComment())?  ( SegmentDeclaration() | BlockDeclaration() ) )+
  <EOF>

 { return jjtThis;}
}

void FormalComment() : 
{Token comment = null;}
{   
    comment =<FORMAL_COMMENT>
    { jjtThis.value = comment.image; }
}

void BlockDeclaration():
{Token id = null;}
{
  <BLOCK> id = <IDENTIFIER> <LBRACE>
    ( GenericParameterDeclaration() <SEMICOLON> )?
    ( (FormalComment())? EntityDeclaration() )+   
    <RBRACE>
    { jjtThis.value = id.image; }
}


void SegmentDeclaration():
{Token id = null;}
{
  <SEGMENT> id = <IDENTIFIER> <LBRACE>
    ( GenericParameterDeclaration() <SEMICOLON> )?
    ( (FormalComment())? EntityDeclaration() )+   
    <RBRACE>
    { jjtThis.value = id.image; }
}

void EntityDeclaration():
{Token id = null;}
{
  <ENTITY>  id = <IDENTIFIER> ( InheritanceDeclaration() )? <LBRACE>
    ( GUIDId())?
    ( VariableDeclaration() )*
  <RBRACE>
    { jjtThis.value = id.image; }
 }

void GUIDId() :
{Token guid= null;}
{
    <OBJECT_TYPE_ID> <COLON> guid=<GUID_LITERAL> <SEMICOLON>
   { jjtThis.value = guid.image; }
}

void InheritanceDeclaration() : 
{ Token parent = null;}
{
    <EXTENDS> parent = <IDENTIFIER>
    { jjtThis.value = parent.image; }
 }


void VariableDeclaration() :
{Token id = null;
}
{
    <VAR> TypeDeclaration() ( ArrayDeclaration() )? id = <IDENTIFIER>  ( GenericParameterDeclaration() )? <SEMICOLON>
    { jjtThis.value = id.image; }
}


void ArrayDeclaration() : 
{}
{
   "[]"
}

void TypeDeclaration() : 
{Token id = null;}
{
   (     SimpleTypeDeclaration() 
    |   id = <IDENTIFIER> 
   )

    { if ( id != null) {jjtThis.value = id.image; } }
}

void SimpleTypeDeclaration() : 
{Token simple = null;}
{
    (
      simple= <UChar> 
    | simple= <U8> 
    | simple= <U16> 
    | simple= <U32> 
    | simple= <U64> 
    | simple= <I16> 
    | simple= <I32> 
    | simple= <I64> 
    | simple= <F32> 
    | simple= <F64> 
    | simple = <GUID>
    | simple = <VecF32>
    | simple = <BBoxF32>
    | simple = <CoordF32>
    | simple = <MbString>
    )

        { jjtThis.value = simple.image; }
}

void GenericParameterDeclaration() : 
{}
{
    <LBRACE> ParameterNameAndValue() (  "," ParameterNameAndValue() )*  <RBRACE>    
}

void ParameterNameAndValue() : 
{Token id = null;}
{
    id = <IDENTIFIER> <COLON> ParameterValue()
        { jjtThis.value = id.image; }
}

void ParameterValue() : 
{Token vals = null;}
{
    
      vals = <INTEGER_LITERAL> { jjtThis.value =  Integer.parseInt( vals.image);}
    | vals = <FLOATING_POINT_LITERAL> { jjtThis.value =  Double.parseDouble( vals.image);}
    | vals = <STRING_LITERAL> { jjtThis.value =  vals.image;}
    | vals = <BOOLEAN_LITERAL> { jjtThis.value =  Boolean.parseBoolean(vals.image);}

}




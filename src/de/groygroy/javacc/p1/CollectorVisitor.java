/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.groygroy.javacc.p1;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cz
 */
public class CollectorVisitor implements JoveDLParserVisitor {
    
    public static Logger log = LoggerFactory.getLogger( CollectorVisitor.class );
    
    private String lastComment;

    public Object visit( SimpleNode node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTJoveDL node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTFormalComment node, Map<String, Object> data ) {
        lastComment = ( String ) node.jjtGetValue();
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTBlockDeclaration node, Map<String, Object> data ) {
        lastComment = null;
        node.childrenAccept( this, data );
        return data;
    }

    public SegmentDeclaration visit( ASTSegmentDeclaration node, Map<String, Object> data ) {
        String segmentName = ( String ) node.jjtGetValue();        

        SegmentDeclaration retval = new SegmentDeclaration();
        retval.setName( segmentName );
        retval.setComment( lastComment );
        lastComment = null;

        for ( int i = 0; i < node.jjtGetNumChildren(); i++ ) {
            Node child = node.jjtGetChild( i );
            

            if ( child instanceof ASTEntityDeclaration ) {
                Object val = child.jjtAccept( this, data );
                retval.getEntities().add( ( EntityDeclaration ) val );
            } else {
                // found another AST child, e.g. a variable declaration
            }
        }

        log.info( "created " + retval );
        return retval;
    }

    public EntityDeclaration visit( ASTEntityDeclaration node, Map<String, Object> data ) {
        // Use the entity name from the AST node and create a new POJOs
        
        String entityName = ( String ) node.jjtGetValue();
        EntityDeclaration retval = new EntityDeclaration();
        retval.setName( entityName );
        retval.setComment( lastComment );
        lastComment = null;
        log.info( "created " + retval );
        return retval;
    }

    public Object visit( ASTGUIDId node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTInheritanceDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTVariableDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTArrayDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTTypeDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTSimpleTypeDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTGenericParameterDeclaration node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTParameterNameAndValue node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }

    public Object visit( ASTParameterValue node, Map<String, Object> data ) {
        node.childrenAccept( this, data );
        return data;
    }
}

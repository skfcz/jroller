package de.groygroy.cmgs;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.jvnet.substance.skin.SubstanceBusinessLookAndFeel;
import org.jvnet.substance.skin.SubstanceModerateLookAndFeel;


public class FormsBuilderDemo {

    public static final void main( String[] args ) {

        SwingUtilities.invokeLater( new Runnable() {

                public void run() {
                    try {
                        UIManager.setLookAndFeel(new SubstanceModerateLookAndFeel());
                    


//                    try {
//
//                        if ( LookUtils.IS_OS_WINDOWS ) {
//                            UIManager.setLookAndFeel(
//                                new com.jgoodies.looks.windows.WindowsLookAndFeel() );
//                        } else if ( LookUtils.IS_OS_WINDOWS_XP ) {
//                            UIManager.setLookAndFeel(
//                                new com.jgoodies.looks.plastic.PlasticXPLookAndFeel() );
//                        } else {
//                            UIManager.setLookAndFeel(
//                                new com.jgoodies.looks.plastic.PlasticLookAndFeel() );
//                        }
                    } catch ( Exception exp ) {

                    }

                    JFrame frame = new JFrame( "Carsten Zerbst # FormsBuilderDemo" );

                    JPanel form = new FormsBuilderDemo().createComponent();
                    frame.getContentPane().add( form );

                    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

                    frame.pack();
                    frame.setVisible( true );

                }
            } );

    }

    public JPanel createComponent() {

        FormLayout layout = new FormLayout(
                "4dlu,right:max(50dlu;pref), 4dlu, 75dlu:grow, 4dlu, pref, 4dlu",
                "4dlu,pref,3dlu,pref,3dlu,pref,fill:4dlu:grow" ); // rows

        PanelBuilder builder = new PanelBuilder( layout, new FormDebugPanel() );
        CellConstraints cc = new CellConstraints();

        int row = 2;
        builder.addLabel( "Label 1 :", cc.xy( 2, row ) );

        JTextField field1 = new JTextField();
        builder.add( field1, cc.xyw( 4, row, 3 ) );

        row += 2;
        builder.addLabel( "Longer Label :", cc.xy( 2, row ) );

        JTextField field2 = new JTextField();
        builder.add( field2, cc.xyw( 4, row, 3 ) );

        row += 2;
        builder.addLabel( "Label 3:", cc.xy( 2, row ) );

        JTextField field3 = new JTextField();
        builder.add( field3, cc.xy( 4, row ) );

        JButton select = new JButton( "..." );
        builder.add( select, cc.xy( 6, row ) );

        return builder.getPanel();

    }

}
